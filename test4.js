/*
  What is the result of the following code?
  Explain your answer.
*/

function printing() {
  console.log(1);

  setTimeout(function() {
    console.log(2);
  }, 1000);

  setTimeout(function() {
    console.log(3);
  }, 0);

  console.log(4);
}
printing();

/* Since setTimeout() runs asynchronously, the 2 setTimeout() function will print the log after the other function executed. Therefore the log will be print as following:
  1
  4 // this runs right after 1, due to the execution order
  3 // this runs right after all other functions get executed
  2 // same as 3, but after 1 seconds after called.
*/
