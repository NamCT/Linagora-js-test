function test() {
  console.log(a);
  console.log(foo());
  var a = 1;
  function foo() {
    return a;
  }
}
test();

/* 
  The first log in line 2 will result in "undefined" because the variable "a" now has not been declared. Though in line 4, the variable "a" is defined, but only the declaration of variable a has been *hoisted*, not the value of "a". Therefore "a" remains undefined at line 2.

  The result is still "undefined". In line 3, function "foo" is called, and return value of "a", but "foo" runs synchronously, meaning that after the call in line 3, it has to finish the call of function "foo" in line 5 before move onto line 4. As explained above, "a" variable still has not been given a value, and JS considers this as "undefined" due to JS hoisting.
*/
