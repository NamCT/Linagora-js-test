/*
Define a repeatify function on the String
object. The function accepts an integer that
specifies how many times the string has to be
repeated. The function returns the string
repeated the number of times specified.
For example:

console.log('hello'.repeatify(3)); 

Should print hellohellohello.
*/

// using loop
function stringRepeatify(str, int) {
  // todo: verify the string and int are valid
  let arr = [];
  for (let i = 0; i < int; i++) {
    arr.push(str);
  }
  console.log("" + arr.join(""));
}
// stringRepeatify("Linagora", 3); // LinagoraLinagoraLinagora

// using recursion
var string = "";
var repeatifyByRecursion = function(str, int) {
  // todo: check if the string and int are valid
  if (int > 0) {
    string += str;
    return repeatifyByRecursion(str, int - 1);
  } else {
    return string;
  }
};
// repeatifyByRecursion("Linagora", 3); // LinagoraLinagoraLinagora

// extends String object
String.prototype.repeatify = function(int) {
  // we could wrap recursionRepeadtify into this.
  return repeatifyByRecursion(this.valueOf(), int);
};

console.log("hello".repeatify(3));
