/* 1/ What is the result of the following code?
 Explain your answer.
*/
var fullname = "John Doe"; // this refers to global object "window"
var obj = {
  fullname: "Colin Ihrig",
  prop: {
    fullname: "Aurelio De Rosa",
    getFullname: function() {
      return this.fullname;
    }
  }
};
console.log(obj.prop.getFullname()); // this prints Aurelio De Rosa
var test = obj.prop.getFullname;
console.log(test()); // this prints John Doe

/*
  Explaination:

  Line 14: obj.prop.getFullname() will result in Aurelio De Rosa because the "this" context refers to the "prop" object, which is a property of object "obj", not the same as fullname property in "obj" object.
  Line 16: test was defined in line 15, then called in line 16. The "this" context in getFullname method now refers to global object "window", so it points to "John Doe" instead. 

  The value of "this" will be determined where the function is called, not where the function is defined.

  Note: The "this" context in getFullname method could be point to "prop" object or "obj" object using bind(): 
    var test = obj.prop.getFullname.bind(obj); 
    test(); // prints Colin Ihrig instead.
*/
