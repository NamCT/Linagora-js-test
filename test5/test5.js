var app = angular.module("LinagoraTest", []);

app.config(function($compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(blob):/);
});

// this directive was taken from sqren
// ref: https://stackoverflow.com/a/19647381
app.directive("fileInput", function() {
  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      var fileChangedHandler = scope.$eval(attrs.fileInput);
      element.bind("change", fileChangedHandler);
    }
  };
});

app.controller("attachmentsController", [
  "$scope",
  function($scope) {
    $scope.filesInput = [];

    $scope.onFileChanged = function(event) {
      var files = event.target.files,
        acceptedType = ["PNG", "JPG", "JPEG", "MP3", "MP4", "PDF", "DOCX"];

      console.log(files);
      // could not treat FileList object as array
      Array.prototype.forEach.call(files, function(file) {
        // file.type would result in image/jpg or application/xml, that's why we need a new prop
        file.extension = file.name.substring(file.name.lastIndexOf(".") + 1);
      });

      for (var i = 0; i < files.length; i++) {
        if (acceptedType.indexOf(files[i].extension.toUpperCase()) > -1) {
          $scope.filesInput.push(files[i]);
          $scope.$apply();
        } else {
          alert(files[i].extension + " extension is not supported");
          /*
            Do something here to prevent submitting. Or maybe put this loop inside the submit handler. The display would still be the number of selected items, since its browser's job.
          */
          continue;
        }
      }
      if ($scope.filesInput) {
        $scope.filesInput.forEach(function(file, index) {
          previewFile(file, index);
        });
      }
    };

    //todo: this need fixed
    function previewFile(file, index) {
      var ext = file.extension.toUpperCase();
      var scope = angular
        .element(document.getElementById("file-preview"))
        .scope();
      var url = URL.createObjectURL(file);

      // this piece of codes need rework/restructure.
      if (ext == "PNG" || ext == "JPG" || ext == "JPEG") {
        scope.filesInput[index].imgSrc = url;
        scope.filesInput[index].src = scope.filesInput[index].imgSrc;
      } else if (ext == "MP3") {
        scope.filesInput[index].audioSrc = url;
        scope.filesInput[index].src = scope.filesInput[index].audioSrc;
        scope.filesInput[index].onend = function(e) {
          URL.revokeObjectURL(scope.filesInput[index].audioSrc);
        };
      } else if (ext == "MP4") {
        scope.filesInput[index].videoSrc = url;
        scope.filesInput[index].src = scope.filesInput[index].videoSrc;
        scope.filesInput[index].onend = function(e) {
          URL.revokeObjectURL(scope.filesInput[index].videoSrc);
        };
        //sucks, but logically to the requirements
      } else {
        if (ext == "PDF") {
          scope.filesInput[index].imgSrc = "img/pdf.png";
          scope.filesInput[index].src = url;
        }
        if (ext == "DOCX") {
          scope.filesInput[index].imgSrc = "img/docx.png";
          scope.filesInput[index].src = url;
        }
      }
      scope.$apply();
    }
  }
]);
